FROM hashicorp/terraform:0.12.20

RUN apk add --update python3
RUN pip3 install boto3 --upgrade --user
